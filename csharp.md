# C#

* Source file names are PascalCase.
* Visual Studio prefers spaces as opposed to tabs so that's what I use for C# source code.
* Razor view comments look like this: `@* Populate dropdown box for make of radio. *@`
* I generally avoid using those summary type comments that look like this as they take up too much space... four lines instead of a one line comment:

```csharp
/// <summary>
/// Does some magic stuff
/// <see cref="DoesMagic"/> class.
/// </summary>
```

* Braces, the opening and closing curly bracket is on a new line. This is what Visual Studio prefers.

```csharp
if (x == true)
{
	// Do something.
}
```
