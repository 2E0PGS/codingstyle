# Coding style

This document covers my base coding style standardisation. I program across quite a few languages so this base style tries to layout core principles.

## Naming schemes

If you have two or fewer name **acronym** it's all capitalised: [dotnet/standard/design-guidelines/capitalization-conventions](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/capitalization-conventions)

```csharp
public int RequestId { get; set; } // Id isn't an acronym.

using System.IO;

using System.Web.UI;

private void MyMethodThatWillInsertDB() {}
```

However under no cases do camelCase identifiers start with a capital:

```csharp
string dbVariableName = "foo";
```

Three letter name acronyms are not capitalised:

```csharp
using System.Data.SqlClient;
```

### Summary

* Do capitalize both characters of two-character acronyms, except the first word of a camel-cased identifier.
* Do capitalize only the first character of acronyms with three or more characters, except the first word of a camel-cased identifier.
* Do not capitalize any of the characters of any acronyms, whatever their length, at the beginning of a camel-cased identifier.

## Braces

The opening curly bracket is on same line as if statement and the closing on its own:

```csharp
if (x == true) {
	// Do something.
}
```

## IDE

* I use a tab display width of 4 spaces.
* I use actual tabs. Apart from in languages that don't like tabs.
* I wrap around 120 ish columns text length.

## Comments

* Comments are dependent on language. Most languages modern style comments are used.
* Use standard English. So starts with capital letter and ends in fullstop (unless they are spacers, commented out code or end in code snippets).
* Notice how the comment has a space before the text. This helps distinguish  commented out code and regular comments.

```csharp
/* Old style */

// New style.

//private void ExampleCommentedOutCode() {

/*
* Multi-Line comments look like this
* because foobar today and
* foobar tomorrow.
*/

// TODO: This is a todo comment.
```

Avoid noisy comments like this.

```csharp

////////// comment

********************comment*******************

```

## Git commit messages

* Use standard English. So starts with capital letter and ends in fullstop (if possible due to character limit in "Summary" field).

## Git repo names

* No spaces.
* They are lowercase and hyphen-case unless the project is a fork or it requires a special name for example: Github pages.

## Git branch names

* No spaces.
* They are lowercase and hyphen-case besides the prefix which has a forward slash delimiter. For example: `f/add-aws-support`
* For more information see my repo: [Git Branching Model](https://bitbucket.org/2E0PGS/git-branch-model/)

## Source file names

* No spaces.
* Named after the class name.
* They are lowercase and hyphen-case.

## Source folder names

* No spaces.
* They are lowercase and hyphen-case.

## Binary file names

* Linux C binaries and other binaries should be hyphen-case.
* All shell (.sh) and batch (.bat) scripts are hyphen-case.

## Tabs

* Tabs are for indentation. Don't use tabs for alignment.
* Spaces are used for alignment since they appear the same on all machines. For example aligning text six spaces to the right for console application print statements.
