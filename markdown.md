# Markdown

* Subheadings should always follow size order to allow for expandability without mass re-factoring other subheadings.
* Bullet points use a asterisk (*) instead of a hyphen (-) for consistency.
* Make use of code block language identifier where possible e.g. \`\`\`csharp or \`\`\`sh
* Keep to one language per a code blocks where possible. Exceptions are if you have a mixed language source file or code that generates more code.