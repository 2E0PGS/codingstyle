# TLDR (Too Long Didn't Read)

* Most languages use camelCase however shell and Python use snake_case.
* Source file names are named after the class name. PascalCase for C# or Java and other languages are lowercase and hyphen-case.
* I use actual tabs. Apart from in C# due to Visual Studio and languages that don't like tabs such as Python.
