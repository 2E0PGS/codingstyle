# Python

* Variable names are all snake_case to separate words. For example: `int example_variable;` See: [pep-0008/#function-and-variable-names](https://www.python.org/dev/peps/pep-0008/#function-and-variable-names)
* Function names are all snake_case to separate words.
* Python uses snake_case for source file names.
* Python uses spaces instead of tabs for indentation. See: [pep-0008/#id17](https://www.python.org/dev/peps/pep-0008/#id17)
* Python uses this style comment: `# Test comment.`
* Python class names are PascalCase. See [pep-0008/#class-names](https://www.python.org/dev/peps/pep-0008/#class-names)
