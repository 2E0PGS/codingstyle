# Shell

* Variable names and function names are all snake_case to separate words. For example: `int example_variable;`
* Shell uses this style comment: `# Test comment.`
