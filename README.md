# Welcome

This is the documentation for my "coding style" aka programming styling standardisation

## Notice

* My coding style documentation will evolve with time. Technologies change and people change. I try to use standards that will last me a life time but that's not always possible and we are only human.
* This repo should really be called: `coding-style`

## About

To quote the Linux Kernel doc: "Coding style is very personal, and I won't force my views on anybody."

I also agree with this matter. I decided that I should layout a standard that I use for my programming. By setting out some standardisation it benefits both me and any contributors who may wish to contribute to my project.

## Languages

* [Arduino](arduino.md)
* [Base coding style](base.md) *(This is the given coding style for all languages I use unless language specific page is provided below)*
* [Batch](batch.md)
* [C#](csharp.md)
* [CSS](css.md)
* [HTML](html.md)
* [Java](java.md)
* [JavaScript](javascript.md)
* [Markdown](markdown.md)
* [Python](python.md)
* [Shell](shell.md)
* [T-SQL](t-sql.md)
* [TLDR](tldr.md)

## Also see

* [codingstyle-c](https://bitbucket.org/2E0PGS/codingstyle-c) C program which prints information about my coding style to terminal.
